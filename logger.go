package main

import "go.uber.org/zap"

var Log zap.SugaredLogger

func SetLogger(newLogger zap.SugaredLogger)  {
	Log = newLogger
}

func InitLogger(env string)  {
	var zapLogger *zap.Logger
	if env == "local" {
		zapLogger,_ = zap.NewDevelopment()
	} else {
		zapLogger,_ = zap.NewProduction()
	}

	SetLogger(*zapLogger.Sugar())
}