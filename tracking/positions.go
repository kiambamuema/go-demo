package tracking

import (
	"context"
	"demoGo/configs"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
)

type LocationData struct {
	ID         primitive.ObjectID   `json:"_id,omitempty"        bson:"_id,omitempty"`
	DeviceId   int64                `json:"device_id,omitempty"   bson:"device_id,omitempty"`
	DeviceName string               `json:"device_name,omitempty" bson:"device_name,omitempty"`
	DeviceType string               `json:"device_type,omitempty" bson:"device_type,omitempty"`
	Device     Device               `json:"location,omitempty"   bson:"location,omitempty"`
	Partner    Partner              `json:"partner,omitempty"    bson:"partner,omitempty"`
}


type Device struct {
	Lat          float64              `json:"lat,omitempty"        bson:"lat,omitempty"`
	Lng          float64              `json:"lng,omitempty"        bson:"lng,omitempty"`
	Direction    string               `json:"direction,omitempty"  bson:"direction,omitempty"`
	Speed        float64              `json:"speed,omitempty"      bson:"speed,omitempty"`
	VoltageLevel int64                `json:"voltage_level,omitempty"   bson:"voltage_level,omitempty"`
	Imei         string               `json:"imei,omitempty"       bson:"imei,omitempty"`
	Time         time.Time            `json:"time,omitempty"       bson:"time,omitempty"`

}





func CreatePositionDataEndPoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	var data LocationData
	_  = json.NewDecoder(req.Body).Decode(&data)
	collection := configs.Client.Database("tracking").Collection("positions")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	result,err := collection.InsertOne(ctx,data)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	log.Println("positions data created")
	_ = json.NewEncoder(res).Encode(result)
}

func GetPositionDataEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	params := mux.Vars(req)
	id,_ := primitive.ObjectIDFromHex(params["id"])
	var data LocationData
	collection := configs.Client.Database("tracking").Collection("positions")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	err := collection.FindOne(ctx,LocationData{ID: id}).Decode(&data)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
        return
	}

	_ = json.NewEncoder(res).Encode(data)
}

func GetPositionsDataEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	var locations []LocationData
	collection := configs.Client.Database("tracking").Collection("positions")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	cursor, err := collection.Find(ctx,bson.M{})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var data LocationData
		_ = cursor.Decode(&data)
		locations = append(locations,data)

	}
	if err := cursor.Err(); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	log.Println("positions data retrieved")
	_ = json.NewEncoder(res).Encode(locations)
}

func DeletePositionsEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	params := mux.Vars(req)
	id,_ := primitive.ObjectIDFromHex(params["imei"])
	collection := configs.Client.Database("tracking").Collection("positions")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	result,err := collection.DeleteOne(ctx,Tracker{ID:id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"message": "`+err.Error() +`"}`))
		return
	}
	json.NewEncoder(res).Encode(result)
}