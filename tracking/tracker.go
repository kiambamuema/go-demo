package tracking

import (
	"context"
	"demoGo/configs"
	"encoding/json"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"time"
)




type Tracker struct {
	ID           primitive.ObjectID   `json:"_id,omitempty"        bson:"_id,omitempty"`
	DeviceId   int64                `json:"device_id,omitempty"   bson:"device_id,omitempty"`
	DeviceName string               `json:"device_name,omitempty" bson:"device_name,omitempty"`
	SimCardNo string               `json:"sim_card_no,omitempty" bson:"sim_card_no,omitempty"`
	DeviceType string               `json:"device_type,omitempty" bson:"device_type,omitempty"`
	Imei         string               `json:"imei,omitempty"       bson:"imei,omitempty"`
}

func CreateDeviceEndpoint(res http.ResponseWriter, req *http.Request)  {

	res.Header().Set("content-type", "application/json")
	var device Tracker
	_ = json.NewDecoder(req.Body).Decode(&device)
	collection := configs.Client.Database("tracking").Collection("tracker")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	result,_ := collection.InsertOne(ctx,device)
	json.NewEncoder(res).Encode(result)


}
func GetDeviceEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	params := mux.Vars(req)
	imei,_ :=params["imei"]
	var device Tracker
	collection := configs.Client.Database("tracking").Collection("tracker")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	err := collection.FindOne(ctx,Tracker{Imei: imei}).Decode(&device)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"message": "`+ err.Error() +`"}`))
		return
	}
  json.NewEncoder(res).Encode(device)
}
func GetDevicesEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	var devices []Tracker
	collection := configs.Client.Database("tracking").Collection("tracker")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	cursor, err := collection.Find(ctx,bson.M{})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"message": "`+err.Error() +`"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var device Tracker
		cursor.Decode(&device)
		devices = append(devices,device)

	}
	if err := cursor.Err(); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"message": "`+err.Error() +`"}`))
		return
	}
	json.NewEncoder(res).Encode(devices)
}
func UpdateDeviceEndpoint(res http.ResponseWriter, req *http.Request)  {

}

func DeleteDeviceEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	params := mux.Vars(req)
	id,_ := primitive.ObjectIDFromHex(params["imei"])
	collection := configs.Client.Database("tracking").Collection("tracker")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	result,err := collection.DeleteOne(ctx,Tracker{ID:id})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		res.Write([]byte(`{"message": "`+err.Error() +`"}`))
		return
	}
	json.NewEncoder(res).Encode(result)
}