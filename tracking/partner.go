package tracking

import (
	"context"
	"demoGo/configs"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"time"
)

type Partner struct {
	RiderId        int                `json:"rider_id,omitempty"       bson:"rider_id,omitempty"`
	RiderName      string             `json:"rider_name,omitempty"     bson:"rider_name,omitempty"`
	LicenseStatus  int                `json:"license_status,omitempty" bson:"license_status,omitempty"`
	CarrierType    int                `json:"carrier_type,omitempty"   bson:"carrier_type,omitempty"`
	VendorType     int                `json:"vendor_type,omitempty"    bson:"vendor_type,omitempty"`
	Owner          Owner              `json:"owner,omitempty"          bson:"owner,omitempty"`
	Vehicle        Vehicle            `json:"vehicle,omitempty"        bson:"vehicle,omitempty"`
	Messages       []Messages         `json:"messages,omitempty"      bson:"messages,omitempty"`

}

type Owner struct {
	OwnerId       int                  `json:"owner_id,omitempty"          bson:"owner_id,omitempty"`
	OwnerName     string               `json:"owner_name,omitempty"        bson:"owner_name,omitempty"`
	OwnerPhone    string               `json:"owner_phone,omitempty"       bson:"owner_phone,omitempty"`


}

type Vehicle struct {
	VehicleId     int                   `json:"vehicle_id,omitempty"          bson:"vehicle_id,omitempty"`
	RegistrationNo string               `json:"reg_no,omitempty"              bson:"reg_no,omitempty"`

}

type Messages struct {
    SenderID       string `json:"sender_id" bson:"sender_id"`
	MessageID      string `json:"message_id" bson:"message_id"`
    ContentMessage string `json:"content_message" bson:"content_message"`
    CreatedAt      string `json:"created_at" bson:"created_at"`


}

func CreatePartnerDataEndPoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	var data Partner
	_  = json.NewDecoder(req.Body).Decode(&data)
	collection := configs.Client.Database("tracking").Collection("partner")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	result,err := collection.InsertOne(ctx,data)
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	_ = json.NewEncoder(res).Encode(result)
}

func GetPartnersDataEndpoint(res http.ResponseWriter, req *http.Request)  {
	res.Header().Set("content-type", "application/json")
	var partners []Partner
	collection := configs.Client.Database("tracking").Collection("partner")
	ctx, _ := context.WithTimeout(context.Background(),5*time.Second)
	cursor, err := collection.Find(ctx,bson.M{})
	if err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var partner Partner
		_ = cursor.Decode(&partner)
		partners = append(partners,partner)

	}
	if err := cursor.Err(); err != nil {
		res.WriteHeader(http.StatusInternalServerError)
		_, _ = res.Write([]byte(`{"message": "` + err.Error() + `"}`))
		return
	}
	_ = json.NewEncoder(res).Encode(partners)
}
