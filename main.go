package main

import (
	"context"
	"demoGo/configs"
	inject "demoGo/tracking"
	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/zap"
	"gopkg.in/mgo.v2/bson"
	"log"
	"net/http"
	"time"
)



const (
	MONGODBURL = "mongodb://localhost:27017"
	POST = "POST"
	GET = "GET"
	UPDATE = "PUT"
	DELETE = "DELETE"
	PORT = "9094"
	DEVICES = "/device"

)

var (
	logger      *zap.SugaredLogger
)

func main()  {
	InitLogger("local")
	log.Println("Demo Go is running")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	clientOptions := options.Client().ApplyURI(MONGODBURL)
	configs.Client, _ = mongo.Connect(ctx,clientOptions)
	defer configs.Client.Disconnect(ctx)
	databases, err := configs.Client.ListDatabaseNames(ctx, bson.M{})
	if err != nil {
		log.Fatal(err.Error())
	}
	log.Println(databases)
	router := mux.NewRouter()
	// tracker endpoints
	router.HandleFunc(DEVICES, inject.CreateDeviceEndpoint).Methods(POST)
	router.HandleFunc(DEVICES, inject.GetDevicesEndpoint).Methods(GET)
	router.HandleFunc(DEVICES +"/{imei}", inject.GetDeviceEndpoint).Methods(GET)
	router.HandleFunc(DEVICES, inject.UpdateDeviceEndpoint).Methods(UPDATE)
	router.HandleFunc(DEVICES + "/{id}", inject.DeleteDeviceEndpoint).Methods(DELETE)

	// positions endpoints
	router.HandleFunc("/positions", inject.CreatePositionDataEndPoint).Methods(POST)
	router.HandleFunc("/positions", inject.GetPositionsDataEndpoint).Methods(GET)
	router.HandleFunc("/positions/{id}", inject.GetPositionDataEndpoint).Methods(GET)
	router.HandleFunc("/positions/{id}", inject.DeletePositionsEndpoint).Methods(DELETE)

	// partner endpoints
	router.HandleFunc("/partner", inject.CreatePartnerDataEndPoint).Methods(POST)
	router.HandleFunc("/partner", inject.GetPartnersDataEndpoint).Methods(GET)
	//router.HandleFunc("/partner/{id}", inject.GetPositionDataEndpoint).Methods(GET)

	http.ListenAndServe(":" +PORT, router)
}
